---
title: "About artidata"
description: |
  Imaduddin Haetami's Personal Website
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r, out.width="200px"}
knitr::include_graphics("figures/DP.jpeg")
```

Hi! Welcome to My Personal Website :)

I was born to be really good at losing stuff.
I lost count on how many times I have misplaced my wallets, passports, phones and never be able to retrieve it back.
Hence, I decided to create this website, for the purpose of not losing my work.

Here, you can have access to my Shiny apps, R analysis, and data visualization gallery.
My professional experience is documented extensively in my [LinkedIn](https://www.linkedin.com/in/imaduddin-haetami/) profile.
Besides working with data and R, I lost myself in calisthenics, body-building, EDM and soccer.  

Oh, if you think that artidata originates from the word "art" and "data", you are partially correct.
In my undergraduate studies, I spend a large portion of my time proving statistical models and machine learning algorithms.
Those were mathematical science with solid proofs.
I felt that working with data was all about science.

As my career progress, I become less interested with the science. 
I proof and understand less, but I apply more.
I also become more involved in the understanding the operational database, and transforming them into analytical data. 
The needs to communicate numbers with other people also introduce me to the art of data visualization. 
Such skills are less of a science, where things often objective, more of the art, where things often subjective.

Besides, artidata is also a play on word of "arti".
It is a word in Bahasa Indonesia that translates to insight.
Supposed that you or your business collated data, often time is for operational purpose.
My interest will be to make your data to be insightful for the business.
Be it in the form of robust data analytics, or useful predictive modelling, arti is the only thing that I strive for. 

Best regards,

Imaduddin Haetami
