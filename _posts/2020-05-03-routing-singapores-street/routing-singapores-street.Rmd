---
title: "Routing Singapore's Street"
description: |
  Using OpenStreetMaps and R's igraph package to create a routing engine for Singapore's streets.
author:
  - name: Imaduddin Haetami
date: 05-03-2020
output:
  distill::distill_article:
    self_contained: false
    toc: true
---
<style>
body {
text-align: justify}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo=T)
```

## Preamble

This article is about my experience in building a routing engine for streets in Singapore.
Starting from downloading line data from the [openstreetmap](openstreetmap.org)(OSM), constructing the [graph](https://en.wikipedia.org/wiki/Graph_theory) structure from the lines, ended with sampling on the shortest path output of the graph.
In the process, I use [R](https://www.r-project.org/) programming language and packages developed by its community.

I will explain in detail how each package contributes for this project. 
But for now, let's load them first.

```{r}
library(osmdata)
library(sf)
library(dplyr)
library(lwgeom)
library(igraph)
library(mapview)
```

The end product will be the shortest path computation.
I am pretty familiar with R's interface to [igraph](https://igraph.org/r/) package.
I have used that before for routing Singapore's bus and train network.
If you deal with a graph problems, I really feel that igraph is a complete package.
It has Python and C interface too.

I have used igraph in the past.
But in the past, the network lines were given by my supervisor.
The current problem is where do you get the graph?


## Data Abstraction From OSM

Fortunately, I came across osmdata package, which offer a convenient way to download OSM geometries to R.
So, we download the data using [osmdata](https://docs.ropensci.org/osmdata/) package:

```{r,message=F}
if(file.exists("sg highway 20200119b.rds")==F){
  sg=opq(bbox=c(103.614311874285,1.23047931395298,104.031394289329,1.46968609740717)) %>%
    add_osm_feature(key='highway')%>%
    osmdata_sf()
  saveRDS(sg,"sg highway 20200119b.rds") 
  #saving the data and not making multiple attempts on the same query.
}else{
  #load the data if it already exist
  sg=readRDS("sg highway 20200119b.rds") 
}
```

Let's see what we get from the download:

```{r}
summary(sg)
```

Great, it's an sf object from [sf](https://r-spatial.github.io/sf/index.html) packaga.
Looking at 190262 linestrings, it seems that this data will be the robust one. 
So, I will explore this dataset first, and I choose [dplyr](https://dplyr.tidyverse.org/) package for this task.

## Lines Exploration and Cleaning

```{r}
lineSG=sg$osm_lines
```

One of the step for exploration:

```{r}
lineSG %>% pull(highway) %>% unique()
```

Apparrently, there is footpath too.
Great data, but not what I need at the moment.

After several other steps of exploration, I decided that the following features of the dataset are the lines and attributes that we need:

```{r}
lineSG=lineSG %>% 
  filter(highway%in%c("motorway","motorway_link",
                      "primary","primary_link",
                      "secondary","secondary_link",
                      "tertiary","tertiary_link",
                      "residential",
                      #"service",
                      "unclassified",
                      "trunk","trunk_link")) %>% 
  select(osm_id,name,highway,lanes,maxspeed,oneway,geometry)
```

Let's inpect graphically whether the chosen dataset is promising:

```{r}
mapview(lineSG)
```

It takes sometime to look around the map.
We have some parts of Malaysia, that we do not need.
Then, if you zoom in to Sentosa Islands, it seems that some roundabouts are missing.
Apparently, I found them in osm_polygons onbject.
So, I combine the polygons to the line:

```{r}
polygonSG=sg$osm_polygons
```

```{r}
polygonSG=polygonSG %>%
  filter(junction=="roundabout") %>%
  mutate(geometry=st_cast(geometry,"LINESTRING")) %>%
  select(osm_id,name,highway,lanes,maxspeed,oneway,geometry)
```

```{r}
lineSG=rbind(lineSG,polygonSG)
```

Hence, what we have right now, is just a bunch of lines with some attributes in the object of lineSG.

## Street's Attributes Cleaning

There are some missing values on oneway variable.
We have to estimate what the missing values, using some kind of rationale.
Here I checked whether highway types indicates oneway value

```{r}
lineSG %>%
  as_tibble() %>%
  group_by(highway,oneway) %>%
  summarise(n()) %>%
  print(n=Inf,width=Inf)
```

From the data above, I concluded that those with postfix link can be declared as oneway.

```{r}
lineSG=lineSG %>%
  mutate(oneway=case_when(oneway%in%c("yes","Yes") ~ T,
                          oneway%in%c("no","-1") ~ F,
                          is.na(oneway)&grepl("link$",highway) ~ T,
                          is.na(oneway) ~ F))

```

For those that are not oneway, or two ways, I make duplicate with the path's reverse.

```{r}
lineSG=rbind(lineSG,
             lineSG %>% filter(oneway==F) %>% st_reverse())
```

Here, I ensure that the points are connected, such that the end points of the lines are shared.

```{r}
pointSplit=c(lineSG %>% st_startpoint(),
             lineSG %>% st_endpoint())
```

```{r}
lineSG=lineSG %>% st_split(pointSplit)
lineSG=lineSG %>% st_collection_extract("LINESTRING")
# 30 minutes long
```

Naming the start and ending points of lines by their coordinate:

```{r}
lineSG=lineSG %>%
  mutate(start=as.character(st_startpoint(geometry)),
         end=as.character(st_endpoint(geometry)))
```

Getting the line attributes in the right format:

```{r}
lineSG=lineSG %>%
  mutate(distance=as.numeric(st_length(geometry)),
         maxspeed=as.numeric(maxspeed))
```

There are missing max speed too, but I arbitarily choose 40 for now.

```{r}
lineSG %>%
  as_tibble() %>%
  group_by(highway,maxspeed) %>%
  summarise(n())
```

```{r}
lineSG=lineSG %>%
  mutate(maxspeed=if_else(is.na(maxspeed),40,maxspeed)) %>%
  mutate(time=distance/(0.8*maxspeed*1000/3600))
```

```{r}
name=unique(c(lineSG %>% as_tibble() %>% pull(start),
              lineSG %>% as_tibble() %>% pull(end)))
```

## Building the Graph Structure

Now that we have the lines and attributes ready, it's time to build the graph.
A graph structure is simply a collection of vertices connected by edges.
In this case the sreets are the edges and the junctions are the vertices.
Building the graph with igraph:

```{r}
graphSG <- graph_from_data_frame(lineSG %>% select(start,end,name,distance,time,geometry),T)
```

```{r}
graphSG
```

From the output above, what we see is a graph with 72135 vertices and 115067 edges.
This is a directed graph, with edges attributes include distance, time and geometry of the line.

However, I found that the graph is not strongly connected.
Such that, not all the vertice can reach each other.
In reality, this is not the case for Singapore.
Every streets accessible by car should be strongly connected.

Abstracting the strongly connected graph, with the highest number of vertice:

```{r}
cmpn=components(graphSG,"strong")
graphSG=induced_subgraph(graphSG,names(cmpn$membership[cmpn$membership==which.max(cmpn$csize)]))
```

Plotting the graph:

```{r}
graphSG %>%
  get.edge.attribute("geometry") %>%
  st_as_sfc(crs=4326) %>%
  mapview()
```

From the figure above, we can see that parts of Malaysia is no longer attached, which is exactly what we want.
I think the graph structure is ready.
We can finally make queries on the based on vertices of the graph.

```{r}
saveRDS(graphSG,"sg graph 20200513.rds")
```

## Shortest Path Query

The shortest path from the graph can only be queried by the existing junctions (vertices).
In our case, we need to ensure the path query can be done for every point coordinates of Singapore.
One way to do this is by connecting the starting and ending point to the nearest junction.

Hence, we collate all the junction coordinates:

```{r}
vertexGeom=st_as_sfc(lapply(as.list(graphSG%>%vertex_attr("name")), function(x) st_point(eval(parse(text=x)))))
```

Then, we use the st_nearest_feature function from sf package to find nearest junctions to each of starting point and ending point.

```{r}
queryGeom=st_as_sfc(list(st_point(c(103.855,1.45)),
                         st_point(c(103.77,1.31))))

nearest=st_nearest_feature(queryGeom,vertexGeom)

st_distance(queryGeom,queryGeom,by_element=T)
```

Finally query the junctions to the shortest path function.

```{r}
path=shortest_paths(graphSG,
                    nearest[1],nearest[2],
                    weights=E(graphSG)$time,
                    output="epath")
```

The path object can provide many information of the route.
Here we can visualise the path taken:

```{r}
graphSG %>%
  edge_attr("geometry",path$epath[[1]]) %>%
  st_as_sfc(crs=4326) %>%
  mapview()
```

Here we can abstract the road name and its corresponding distance and time taken:

```{r}
pathAttr=tibble(name=graphSG %>% edge_attr("name",path$epath[[1]]),
                distance=graphSG %>% edge_attr("distance",path$epath[[1]]),
                time=graphSG %>% edge_attr("time",path$epath[[1]]))
```

```{r}
pathAttr %>%
  group_by(name) %>%
  summarise(time=sum(time),distance=sum(distance))
```

Summing up the time and distance taken:

```{r}
pathAttr %>% summarise(distance=sum(distance),time=sum(time))
```

You can simply modify the queryGeom object to other coordinates in Singapore.

That's it!
You now have a routing engine for streets in Singapore.
