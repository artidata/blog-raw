---
title: "Need FoR Speed EP1"
description: |
  Replace readLines with data.table::fread(sep=NULL) for reading any text files.
author:
  - name: Imaduddin Haetami
    url: http://artidata.io/blog/about.html
date: 03-26-2019
categories:
  - optimization
  - analysis
preview: need for speed.png
output:
  distill::distill_article:
    self_contained: false
    toc: true
---
<style>
body {
text-align: justify}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = T)
```

## Preamble

I recently encounter a supposedly 16GB csv file.
However, the csv file does not have the same number of comma "," for each line.
The reason is the one who exported the data, does not consider the randomly existing comma(s) in the address columns.
Several method to read the csv files using R, such as utils::read.csv, readr::read_csv, and default data.table::fread will either throw an error or make an incomplete reading.
Hence, I have no choice but to use the readLines and fix the errors.

Unfortunately, I have to do the readLines for several times.
There were several files which such errors or I overwrite the already read object.
Although I work on my office HPC with [16 Cores of Intel Xeon Gold 6144](https://www.intel.com/content/www/us/en/products/processors/xeon/scalable/gold-processors/gold-6144.html) as the processor, the readLines still takes me around 10 minutes to do one reading.
So slow~

## Problem Replication

Let me replicate the problem and objectively compare the performance.
Our data looks very similar with the one below:

```{r}
len <- 2000000
set.seed(240193)
char <- sapply(1:len,
               function(x)
                 paste(sample(c(LETTERS,","),100,replace = T),
                       collapse=""))
```


The first 5 samples are:

```{r}
head(char)
```

This is a character vector with `r len` elements of 100 random letters and commas. 
Then, we write it to the hard drive as a text file:

```{r}
writeLines(char,"char.txt")
```

The size of the written files (bytes):

```{r}
file.size("char.txt")
```

## Objective Comparison


Now, we compare the 2 techniques: 

```{r}
(t1 <- system.time(ReadLines <- readLines("char.txt")))
library(data.table)
(t2 <- system.time(Fread <- fread("char.txt",sep=NULL,header=F)[,V1]))
```

Checking equality of the 2 objects:

```{r}
identical(ReadLines,Fread)
```

Hence, the second method is `r unname(t1[3]/t2[3])` times as fast as the first method. 
In other words, you can save `r unname((t1[3]-t2[3])/t1[3])*100`% of your time by adopting the second method.

I believe the reason for such speed is the built-in parallelization of data.table::fread.  
It manage to utilize the number of cores of my CPU:

```{r}
getDTthreads()
```

[Here](https://github.com/Rdatatable/data.table/wiki/talks/BARUG_201704_ParallelFread.pdf) is further reading on parallelization of data.table::fread. 
In my experience, the number of cores generally increase the reading speed of fread.
However, your hard disk drive reading speed may become the bottleneck and slowing down the process.

Thank you for reading!